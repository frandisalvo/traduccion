CREATE TABLE libro 
(
    id BIGINT IDENTITY PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    autor VARCHAR(100),
    traducido TINYINT NOT NULL
); 

CREATE TABLE oracion 
(
    id BIGINT IDENTITY PRIMARY KEY,
    texto VARCHAR(300) NOT NULL,
    traduccion VARCHAR(300),
    id_libro BIGINT REFERENCES libro(id)
); 

INSERT INTO libro 
(id, nombre, autor, traducido)
VALUES 
(1,'El Aleph','Jorge Luis Borgues',0),
(2,'Inferno','Dan Brawn',1),
(3,'El hobbit','Tolkien',0);

INSERT INTO oracion 
(id, texto, traduccion, id_libro)
VALUES 
(1,'Hello world',null,1),
(5,'My name is Fracisco',null,1),
(6,'Im from Argentina',null,1),
(2,'Only know that dont know enything',null,2),
(3,'Un Pacman en el Savoy',null,2),
(4,'El pueblo',null,3);
