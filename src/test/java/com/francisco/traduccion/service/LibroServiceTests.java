package com.francisco.traduccion.service;

import com.francisco.traduccion.TraduccionApplicationTests;
import com.francisco.traduccion.domain.Libro;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LibroServiceTests extends TraduccionApplicationTests {

    @Autowired
    private LibroService libroService;
    
    @Test
    public void buscarTodos_sinParametros_devuelveTodosLosLibros() {
        
        List<Libro> libros = libroService.buscarTodos();
        
        Libro unLibro = libros.get(0);
        Libro otroLibro = libros.get(1);
        
        assertEquals(3,libros.size());
        assertEquals(1L,unLibro.getId());
        assertEquals("El Aleph",unLibro.getNombre());
        assertEquals("Jorge Luis Borgues",unLibro.getAutor());
        assertFalse(unLibro.isTraducido());
        assertEquals(2L,otroLibro.getId());
        assertEquals("Inferno",otroLibro.getNombre());
        assertEquals("Dan Brawn",otroLibro.getAutor());
        assertTrue(otroLibro.isTraducido());
    }
    
}
