package com.francisco.traduccion.service;

import com.francisco.traduccion.TraduccionApplicationTests;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.francisco.traduccion.domain.Oracion;
import java.util.List;
import java.util.NoSuchElementException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class OracionServiceTests extends TraduccionApplicationTests {

    @Autowired
    private OracionService oracionService;

    @Test
    public void buscarSiguienteSinTraducirPorIdLibro_oracionesDisponibles_DevuelveUnaOracion() {
        List<Oracion> oraciones = oracionService.buscarSiguientesSinTraducirPorIdLibro(2L,0);
        assertNull(oraciones.get(0).getTraduccion());
        assertEquals("Un Pacman en el Savoy", oraciones.get(0).getTexto());
        assertEquals(1, oraciones.size());
    }

    @Test
    public void buscarSiguienteSinTraducirPorIdLibro_oracionesNoDisponibles_DevuelveNull() {
        List<Oracion> oraciones = oracionService.buscarSiguientesSinTraducirPorIdLibro(3L,0);
        assertTrue(oraciones.isEmpty());
    }

}
