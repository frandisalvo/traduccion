package com.francisco.traduccion.service.impl;

import com.francisco.traduccion.domain.Libro;
import com.francisco.traduccion.repository.LibroRepository;
import com.francisco.traduccion.service.LibroService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LibroServiceImpl implements LibroService {

    @Autowired
    private LibroRepository libroRepository;
    
    @Override
    public List<Libro> buscarTodos() {
        return libroRepository.findAll();
    }
    
}
