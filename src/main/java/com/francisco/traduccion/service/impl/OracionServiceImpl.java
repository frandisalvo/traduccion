package com.francisco.traduccion.service.impl;

import com.francisco.traduccion.domain.Oracion;
import com.francisco.traduccion.repository.OracionRepository;
import com.francisco.traduccion.service.OracionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class OracionServiceImpl implements OracionService {

    @Autowired
    private OracionRepository oracionRepository;

    @Value("${com.francisco.traduccion.pagina.tamanio}")
    private int tamanioPagina;

    @Override
    public List<Oracion> buscarSiguientesSinTraducirPorIdLibro(Long idLibro, int pagina) {
        Page<Oracion> oraciones = oracionRepository.findByLibroId(idLibro, new PageRequest(pagina, tamanioPagina));
        return oraciones.getContent();
    }

}
