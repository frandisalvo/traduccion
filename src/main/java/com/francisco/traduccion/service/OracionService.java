package com.francisco.traduccion.service;

import com.francisco.traduccion.domain.Oracion;
import java.util.List;

public interface OracionService {
    
    public List<Oracion> buscarSiguientesSinTraducirPorIdLibro(Long idLibro, int pagina);
    
}
