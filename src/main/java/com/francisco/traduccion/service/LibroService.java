package com.francisco.traduccion.service;

import com.francisco.traduccion.domain.Libro;
import java.util.List;

public interface LibroService {
    
    public List<Libro> buscarTodos();
    
}
