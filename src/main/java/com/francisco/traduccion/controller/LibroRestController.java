package com.francisco.traduccion.controller;

import com.francisco.traduccion.domain.Libro;
import com.francisco.traduccion.service.LibroService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LibroRestController {
    
    @Autowired
    private LibroService libroService;

    @RequestMapping(value = "api/libros", method = RequestMethod.GET)
    public List<Libro> buscarTodos() {
        return libroService.buscarTodos();
    }

}
