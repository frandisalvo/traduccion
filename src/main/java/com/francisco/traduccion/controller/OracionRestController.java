package com.francisco.traduccion.controller;

import com.francisco.traduccion.domain.Oracion;
import com.francisco.traduccion.service.OracionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OracionRestController {

    @Autowired
    private OracionService oracionService;

    @RequestMapping(value = "api/libros/{idLibro}/oraciones", method = RequestMethod.GET)
    public List<Oracion> buscarDisponibles(@PathVariable Long idLibro, @RequestParam int pagina) {
        return oracionService.buscarSiguientesSinTraducirPorIdLibro(idLibro, pagina);
    }
}
