package com.francisco.traduccion.repository;

import com.francisco.traduccion.domain.Libro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LibroRepository extends JpaRepository<Libro, Long> {
    
}
