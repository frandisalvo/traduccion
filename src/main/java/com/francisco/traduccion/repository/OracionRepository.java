package com.francisco.traduccion.repository;

import com.francisco.traduccion.domain.Oracion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OracionRepository extends JpaRepository<Oracion, Long> {
    
    public Page<Oracion> findByLibroId(Long idLibro, Pageable pagina);
    
}
